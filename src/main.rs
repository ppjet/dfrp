//
// main.rs
// Copyright (C) 2017 Maxime “pep” Buquet <pep@collabora.com>
// Distributed under terms of the LGPLv2.1+ License
//

#![feature(try_from)]

#[macro_use]
extern crate clap;

#[macro_use]
extern crate log;
extern crate env_logger;

extern crate dmarc_parser;

use std::fs::File;
use std::path::Path;
use std::io::Error as IOError;
use std::io::prelude::*;
use std::convert::TryFrom;

use dmarc_parser::dmarc_feedback::DMARCFeedback;

fn read_file<P: AsRef<Path>>(path: P) -> Result<String, IOError> {
    let mut fh = File::open(path).unwrap();
    let mut file = String::new();

    fh.read_to_string(&mut file)?;
    Ok(file)
}

fn main() {
    env_logger::init().unwrap();

    let matches = clap_app!(@app (app_from_crate!())
        (@arg file: -f --file +takes_value +required "File to parse.")
    ).get_matches();

    let filename = matches.value_of("file").unwrap();
    let report = read_file(filename).unwrap();

    debug!("DMARC Raw Report: {}", report);

    debug!("{:#?}", DMARCFeedback::try_from(&report));
}
