//
// report_metadata.rs
// Copyright (C) 2017 Maxime “pep” Buquet <pep@collabora.com>
// Distributed under terms of the LGPLv2.1+ License
//

use std::convert::TryFrom;
use minidom::Element;

use ns;
use errors::Error;
use date_range::DateRange;

#[derive(Debug)]
pub struct ReportMetadata {
    pub org_name: String,
    pub email: String,
    pub extra_contact_info: Option<String>,
    pub report_id: String,
    pub date_range: DateRange,
    pub error: Vec<String>,
}

impl TryFrom<Element> for ReportMetadata {
    type Error = Error;

    fn try_from(root: Element) -> Result<ReportMetadata, Error> {
        if !root.is("report_metadata", ns::DMARC_01) {
            return Err(Error::ParseError("This is not a 'report_metadata' element."));
        }

        Ok(ReportMetadata {
            org_name: match root.get_child("org_name", ns::DMARC_01) {
                Some(v) => v.text().parse()?,
                None => return Err(Error::ParseError("report_metadata requires a 'org_name' child.")),
            },
            email: match root.get_child("email", ns::DMARC_01) {
                Some(v) => v.text().parse()?,
                None => return Err(Error::ParseError("report_metadata requires a 'email' child.")),
            },
            extra_contact_info: match root.get_child("extra_contact_info", ns::DMARC_01) {
                Some(v) => Some(v.text().parse()?),
                None => None,
            },
            report_id: match root.get_child("report_id", ns::DMARC_01) {
                Some(v) => v.text().parse()?,
                None => return Err(Error::ParseError("report_metadata requires a 'report_id' child.")),
            },
            date_range: DateRange::try_from(
                if let Some(elem) = root.get_child("date_range", ns::DMARC_01) {
                    elem.clone()
                } else {
                    return Err(Error::ParseError("date_range element required in report_metadata."));
                }
            )?,
            error: vec![],
        })
    }
}
