//
// date_range.rs
// Copyright (C) 2017 Maxime “pep” Buquet <pep@collabora.com>
// Distributed under terms of the LGPLv2.1+ License
//

use std::convert::TryFrom;
use minidom::Element;

use ns;
use errors::Error;

#[derive(Debug, PartialEq)]
pub struct DateRange {
    pub begin: i32,
    pub end: i32,
}

impl TryFrom<Element> for DateRange {
    type Error = Error;

    fn try_from (root: Element) -> Result<DateRange, Error> {
        if !root.is("date_range", ns::DMARC_01) {
            return Err(Error::ParseError("This is not a 'date_range' element."));
        }

        Ok(DateRange {
            begin: match root.get_child("begin", ns::DMARC_01) {
                Some(v) => v.text().parse()?,
                None => return Err(Error::ParseError("date_range requires a 'begin' child.")),
            },
            end: match root.get_child("end", ns::DMARC_01) {
                Some(v) => v.text().parse()?,
                None => return Err(Error::ParseError("date_range requires a 'end' child.")),
            },
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use minidom::Element;

    #[test]
    fn test_date_range_invalid_root() {
        let elem: Element = "<not_date_range xmlns='http://dmarc.org/dmarc-xml/0.1' />".parse().unwrap();
        let error = DateRange::try_from(elem).unwrap_err();
        assert_eq!(match error {
            Error::ParseError(string) => string,
            _ => panic!(),
        }, "This is not a date_range element.");
    }

    #[test]
    fn test_date_range_invalid_missing_begin() {
        let elem: Element = "<date_range xmlns='http://dmarc.org/dmarc-xml/0.1'>
            <foo/>
            <end>1234567890</end>
        </date_range>".parse().unwrap();
        let error = DateRange::try_from(elem).unwrap_err();
        assert_eq!(match error {
            Error::ParseError(string) => string,
            _ => panic!(),
        }, "date_range requires a 'begin' child.");
    }

    #[test]
    fn test_date_range_invalid_missing_end() {
        let elem: Element = "<date_range xmlns='http://dmarc.org/dmarc-xml/0.1'>
            <begin>1234567890</begin>
            <foo/>
        </date_range>".parse().unwrap();
        let error = DateRange::try_from(elem).unwrap_err();
        assert_eq!(match error {
            Error::ParseError(string) => string,
            _ => panic!(),
        }, "date_range requires a 'end' child.");
    }

    #[test]
    fn test_date_range() {
        let elem: Element = "<date_range xmlns='http://dmarc.org/dmarc-xml/0.1'>
            <begin>1234567890</begin>
            <end>1234567899</end>
        </date_range>".parse().unwrap();
        let date_range = DateRange::try_from(elem);
        let valid_daterange = DateRange {
            begin: 1234567890,
            end: 1234567899,
        };
        assert_eq!(date_range.unwrap(), valid_daterange);
    }
}
