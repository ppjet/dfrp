//
// errors.rs
// Copyright (C) 2017 Maxime “pep” Buquet <pep@collabora.com>
// Distributed under terms of the LGPLv2.1+ License
//

use std::io;
use std::num::{ParseIntError, ParseFloatError};
use std::string::ParseError as ParseStringError;
use minidom::Error as MinidomError;

#[derive(Debug)]
pub enum Error {
    ParseError(&'static str),
    ParseIntError(ParseIntError),
    ParseFloatError(ParseFloatError),
    ParseStringError(ParseStringError),
    IOError(io::Error),
    XMLError(MinidomError),
}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Error {
        Error::IOError(err)
    }
}

impl From<MinidomError> for Error {
    fn from(err: MinidomError) -> Error {
        Error::XMLError(err)
    }
}

impl From<ParseIntError> for Error {
    fn from(err: ParseIntError) -> Error {
        Error::ParseIntError(err)
    }
}

impl From<ParseFloatError> for Error {
    fn from(err: ParseFloatError) -> Error {
        Error::ParseFloatError(err)
    }
}

impl From<ParseStringError> for Error {
    fn from(err: ParseStringError) -> Error {
        Error::ParseStringError(err)
    }
}
