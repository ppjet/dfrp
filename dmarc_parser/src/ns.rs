//
// ns.rs
// Copyright (C) 2017 Maxime “pep” Buquet <pep@collabora.com>
// Distributed under terms of the LGPLv2.1+ License
//

/// RFC 7489: Domain-based Message Authentication, Reporting, and Conformance (DMARC)
pub const DMARC_01: &'static str = "http://dmarc.org/dmarc-xml/0.1";
