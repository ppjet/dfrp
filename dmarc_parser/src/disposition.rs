//
// disposition.rs
// Copyright (C) 2017 Maxime “pep” Buquet <pep@collabora.com>
// Distributed under terms of the LGPLv2.1+ License
//

use std::convert::TryFrom;
use minidom::Element;

use errors::Error;

#[derive(Debug, PartialEq)]
pub enum Disposition {
    None,
    Quarantine,
    Reject,
}

impl TryFrom<Element> for Disposition {
    type Error = Error;

    fn try_from (root: Element) -> Result<Disposition, Error> {
        Ok(match root.text().as_ref() {
            "none" => Disposition::None,
            "quarantine" => Disposition::Quarantine,
            "reject" => Disposition::Reject,
            _ => return Err(Error::ParseError(
                "elements of type DispositionType can only contain 'none', 'quarantine' or 'reject'"
            )),
        })
    }
}
