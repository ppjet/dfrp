//
// lib.rs
// Copyright (C) 2017 Maxime “pep” Buquet <pep@collabora.com>
// Distributed under terms of the LGPLv2.1+ License
//

#![feature(try_from)]

extern crate minidom;

pub mod ns;
pub mod errors;

pub mod version;
pub mod date_range;
pub mod report_metadata;
pub mod alignment;
pub mod disposition;
pub mod policy_published;
pub mod dmarc_feedback;
