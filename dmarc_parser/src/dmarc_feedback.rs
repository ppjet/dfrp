//
// dmarc_feedback.rs
// Copyright (C) 2017 Maxime “pep” Buquet <pep@collabora.com>
// Distributed under terms of the LGPLv2.1+ License
//

use std::convert::TryFrom;
use minidom::Element;

use ns;
use errors::Error;
use version::Version;
use report_metadata::ReportMetadata;
use policy_published::PolicyPublished;

#[derive(Debug)]
pub struct DMARCFeedback {
    pub version: Version,
    pub report_metadata: ReportMetadata,
    pub policy_published: PolicyPublished,
}

impl TryFrom<Element> for DMARCFeedback {
    type Error = Error;

    fn try_from(root: Element) -> Result<DMARCFeedback, Error> {
        if !root.is("feedback", ns::DMARC_01) {
            return Err(Error::ParseError("This is not a 'feedback' element."));
        }

        Ok(DMARCFeedback {
            version: Version::try_from(
                if let Some(elem) = root.get_child("version", ns::DMARC_01) {
                    elem.clone()
                } else {
                    return Err(Error::ParseError("version element required in feedback."));
                }
            )?,
            report_metadata: ReportMetadata::try_from(
                if let Some(elem) = root.get_child("report_metadata", ns::DMARC_01) {
                    elem.clone()
                } else {
                    return Err(Error::ParseError("report_metadata element required in feedback."));
                }
            )?,
            policy_published: PolicyPublished::try_from(
                if let Some(elem) = root.get_child("policy_published", ns::DMARC_01) {
                    elem.clone()
                } else {
                    return Err(Error::ParseError("policy_published element required in feedback."));
                }
            )?,
        })
    }
}

impl<'a> TryFrom<&'a String> for DMARCFeedback {
    type Error = Error;

    fn try_from(xml: &'a String) -> Result<DMARCFeedback, Error> {
        DMARCFeedback::try_from(xml.parse::<Element>()?)
    }
}
