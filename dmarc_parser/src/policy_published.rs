//
// policy_published.rs
// Copyright (C) 2017 Maxime “pep” Buquet <pep@collabora.com>
// Distributed under terms of the LGPLv2.1+ License
//

use std::convert::TryFrom;
use minidom::Element;

use ns;
use errors::Error;

use alignment::Alignment;
use disposition::Disposition;

#[derive(Debug, PartialEq)]
pub struct PolicyPublished {
    domain: String,
    adkim: Option<Alignment>,
    aspf: Option<Alignment>,
    p: Disposition,
    sp: Disposition,
    pct: i64,
    fo: String,
}

impl TryFrom<Element> for PolicyPublished {
    type Error = Error;

    fn try_from (root: Element) -> Result<PolicyPublished, Error> {
        if !root.is("policy_published", ns::DMARC_01) {
            return Err(Error::ParseError("This is not a 'policy_published' element."));
        }

        Ok(PolicyPublished {
            domain: String::new(),
            adkim: if let Some(elem) = root.get_child("adkim", ns::DMARC_01) {
                Some(Alignment::try_from(elem.clone())?)
            } else {
                None
            },
            aspf: if let Some(elem) = root.get_child("aspf", ns::DMARC_01) {
                Some(Alignment::try_from(elem.clone())?)
            } else {
                None
            },
            p: Disposition::try_from(
                if let Some(elem) = root.get_child("p", ns::DMARC_01) {
                    elem.clone()
                } else {
                    return Err(Error::ParseError("p element required in policy_published."));
                }
            )?,
            sp: Disposition::try_from(
                if let Some(elem) = root.get_child("sp", ns::DMARC_01) {
                    elem.clone()
                } else {
                    return Err(Error::ParseError("sp element required in policy_published."));
                }
            )?,
            pct: if let Some(elem) = root.get_child("pct", ns::DMARC_01) {
                elem.text().parse()?
            } else {
                return Err(Error::ParseError("pct element required in policy_published."));
            },
            fo: if let Some(elem) = root.get_child("fo", ns::DMARC_01) {
                elem.text()
            } else {
                return Err(Error::ParseError("fo element required in policy_published."));
            },
        })
    }
}
