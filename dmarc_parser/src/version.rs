//
// version.rs
// Copyright (C) 2017 Maxime “pep” Buquet <pep@collabora.com>
// Distributed under terms of the LGPLv2.1+ License
//

use std::convert::TryFrom;
use minidom::Element;

use ns;
use errors::Error;

#[derive(Debug, PartialEq)]
pub struct Version(f64);

impl TryFrom<Element> for Version {
    type Error = Error;

    fn try_from (root: Element) -> Result<Version, Error> {
        if !root.is("version", ns::DMARC_01) {
            return Err(Error::ParseError("This is not a 'version' element."));
        }

        Ok(Version(root.text().parse()?))
    }
}
