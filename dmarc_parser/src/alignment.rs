//
// alignment.rs
// Copyright (C) 2017 Maxime “pep” Buquet <pep@collabora.com>
// Distributed under terms of the LGPLv2.1+ License
//

use std::convert::TryFrom;
use minidom::Element;

use errors::Error;

#[derive(Debug, PartialEq)]
pub enum Alignment {
    Relaxed,
    Simple,
}

impl TryFrom<Element> for Alignment {
    type Error = Error;

    fn try_from (root: Element) -> Result<Alignment, Error> {
        Ok(match root.text().as_ref() {
            "r" => Alignment::Relaxed,
            "s" => Alignment::Simple,
            _ => return Err(Error::ParseError("elements of type AlignmentType can only contain 'r' or 's'")),
        })
    }
}
