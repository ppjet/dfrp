//
// examples/parse_report.rs
// Copyright (C) 2017 Maxime “pep” Buquet <pep@collabora.com>
// Distributed under terms of the LGPLv2.1+ License
//

#![feature(try_from)]

extern crate dmarc_parser;

use std::convert::TryFrom;

use dmarc_parser::dmarc_feedback::DMARCFeedback;

fn main() {
    let report = String::from(r#"
        <?xml version="1.0" encoding="utf-8"?>
        <feedback xmlns="http://dmarc.org/dmarc-xml/0.1">
          <version>1.2</version>
          <report_metadata>
            <org_name>collabora.co.uk</org_name>
            <email>dmarc@collabora.co.uk</email>
            <extra_contact_info>https://collabora.com</extra_contact_info>
            <report_id>some-report-id</report_id>
            <date_range>
              <begin>1493745506</begin>
              <end>1493749080</end>
            </date_range>
          </report_metadata>
          <policy_published>
            <adkim>r</adkim>
            <aspf>s</aspf>
            <p>reject</p>
            <sp>quarantine</sp>
            <pct>100</pct>
            <fo/>
          </policy_published>
        </feedback>
    "#);

    println!("DMARC Raw Report: {}", report);
    println!("{:#?}", DMARCFeedback::try_from(&report));
}
